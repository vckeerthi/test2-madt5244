package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    int dinosaurXPosition;
    int dinosaurYPosition;

    int poopXposition;
    int poopYposition;

    int candyXposition;
    int candyYposition;

    int rainbowXposition;
    int rainbowYposition;
    Bitmap dinoImage;
    Rect dinohitbox;

    Bitmap poopImage;
    Rect poophitbox;

    Bitmap rainbowImage;
    Rect rainbowhitbox;

    Bitmap candyImage;
    Rect candyhitbox;

    // game state
    boolean gameIsRunning;


    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;
    int score = 0;
    String directiondinoIsMoving = "down";


    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.poopXposition = 30;
        this.poopYposition = 30;

        this.candyXposition = 30;
        this.candyYposition = 190;

        this.rainbowXposition = 30;
        this.rainbowYposition = 330;

        this.dinosaurXPosition = screenWidth-350;
        this.dinosaurYPosition = 350;


        this.dinoImage = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.dino64);
        this.poopImage= BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.poop64);
        this.rainbowImage = BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.rainbow64);
        this.candyImage= BitmapFactory.decodeResource(this.getContext().getResources(),
                R.drawable.candy64);

        this.dinohitbox = new Rect(100,600,100+ dinoImage.getWidth(),600+ dinoImage.getHeight());
        this.poophitbox = new Rect(100,600,100+ poopImage.getWidth(),600+ poopImage.getHeight());
        this.rainbowhitbox = new Rect(100,600,100+ rainbowImage.getWidth(),600+ rainbowImage.getHeight());
        this.candyhitbox = new Rect(100,600,100+ candyImage.getWidth(),600+ candyImage.getHeight());

    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        if (this.fingerAction == "mousedown") {
            this.dinosaurYPosition= this.dinosaurYPosition - 10;

            if (dinosaurYPosition <= 0){
                this.dinosaurYPosition = this.screenHeight/2;
            }
        }

        if (this.fingerAction == "mouseup") {
            this.dinosaurYPosition = this.dinosaurYPosition + 10;


        }
        this.dinohitbox.left  = this.dinosaurXPosition;
        this.dinohitbox.top = this.dinosaurYPosition;
        this.dinohitbox.right  = this.dinosaurXPosition + this.dinoImage.getWidth();
        this.dinohitbox.bottom = this.dinosaurYPosition + this.dinoImage.getHeight();



        if (this.dinohitbox.intersect(this.poophitbox) == true) {
            this.dinosaurXPosition = screenWidth -350;
            this.dinosaurYPosition = 350;

            this.dinohitbox = new Rect(100,
                    600,
                    100+dinoImage.getWidth(),
                    600+dinoImage.getHeight()
            );
            lives = lives - 1;
            score = 0;
        }
        if (this.dinohitbox.intersect(this.candyhitbox) == true) {

            score = score + 1;
         this.candyXposition = 0;
         this.candyYposition = 150;
            this.candyhitbox.left  = this.candyXposition;
            this.candyhitbox.top = this.candyYposition;
            this.candyhitbox.right  = this.candyXposition + this.candyImage.getWidth();
            this.candyhitbox.bottom = this.candyYposition + this.candyImage.getHeight();

        }
        if (this.dinohitbox.intersect(this.rainbowhitbox) == true) {

            score = score + 1;
            this.rainbowXposition = 0;
            this.rainbowYposition = 330;
            this.rainbowhitbox.left  = this.rainbowXposition;
            this.rainbowhitbox.top = this.rainbowYposition;
            this.rainbowhitbox.right  = this.rainbowXposition + this.rainbowImage.getWidth();
            this.rainbowhitbox.bottom = this.rainbowYposition + this.rainbowImage.getHeight();

        }



        this.poopXposition = this.poopXposition + 25;
        this.poophitbox.left  = this.poopXposition;
        this.poophitbox.top = this.poopYposition;
        this.poophitbox.right  = this.poopXposition + this.poopImage.getWidth();
        this.poophitbox.bottom = this.poopYposition + this.poopImage.getHeight();

        if (this.poopXposition >= screenWidth) {
            this.poopXposition = 30;
            this.poopYposition = 30; }

        this.candyXposition = this.candyXposition + 15;
        this.candyhitbox.left  = this.candyXposition;
        this.candyhitbox.top = this.candyYposition;
        this.candyhitbox.right  = this.candyXposition + this.candyImage.getWidth();
        this.candyhitbox.bottom = this.candyYposition + this.candyImage.getHeight();

        if (this.candyXposition >= screenWidth) {
            this.candyXposition = 30;
            this.candyXposition = 30; }

        this.rainbowXposition = this.rainbowXposition + 20;
        this.rainbowhitbox.left  = this.rainbowXposition;
        this.rainbowhitbox.top = this.rainbowYposition;
        this.rainbowhitbox.right  = this.rainbowXposition + this.rainbowImage.getWidth();
        this.rainbowhitbox.bottom = this.rainbowYposition + this.rainbowImage.getHeight();

        if (this.rainbowXposition >= screenWidth) {
            this.rainbowXposition = 30;
            this.rainbowXposition = 30; }





    }
    int lives = 10;

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLACK);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            canvas.drawBitmap(dinoImage, dinosaurXPosition, dinosaurYPosition, paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(poopImage, poopXposition, poopYposition, paintbrush);
            canvas.drawBitmap(rainbowImage, rainbowXposition, rainbowYposition, paintbrush);
            canvas.drawBitmap(candyImage, candyXposition, candyYposition, paintbrush);

            canvas.drawLine(10, 180, screenWidth-450, 180, paintbrush);
            canvas.drawLine(10, 320, screenWidth-450, 320, paintbrush);
            canvas.drawLine(10, 450, screenWidth-450, 450, paintbrush);
            canvas.drawLine(10, 570, screenWidth-450, 570, paintbrush);


            paintbrush.setTextSize(30);
                        canvas.drawText("Lives " + lives, screenWidth-500,80, paintbrush);
            paintbrush.setTextSize(30);
            canvas.drawText("score " + score, screenWidth-700,80, paintbrush);
            canvas.drawRect(this.dinohitbox, paintbrush);
            canvas.drawRect(this.poophitbox, paintbrush);
            canvas.drawRect(this.rainbowhitbox, paintbrush);
            canvas.drawRect(this.candyhitbox, paintbrush);


            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();

        if (userAction == MotionEvent.ACTION_DOWN) {
            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();
            Log.d(TAG, "Person's pressed: "
                    + fingerXPosition + ","
                    + fingerYPosition);
            int middleOfScreen = this.screenHeight / 2;
            if (fingerYPosition <= middleOfScreen) {
                fingerAction = "mousedown";

            }
            else if (fingerYPosition > middleOfScreen) {
                // 4. If tap is on right, racket should go right
                fingerAction = "mouseup";
            }



        }
        else if (userAction == MotionEvent.ACTION_UP) {


        }

        return true;
    }
}
